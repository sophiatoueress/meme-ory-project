### Installation
1. Ouvrir une invite de commande dans le dossier setup\back-end
2. Taper `npm install` pour installer les dépendances requises
3. Si des vulnaribilities sont trouvées, executer "npm audit fix"
4. Ouvrir une invite de commande dans le dossier setup\front-end
5. Taper `npm install` pour installer les dépendances requises
	
 
## Exécution
 
1. Ouvrir une invite de commande dans le dossier setup\back-end
2. Exécuter `npm start` pour run le back-end
3. Ouvrir une invite de commande dans le dossier setup\front-end
4. Exécuter `npm start` pour run le front-end
5. Ouvrir un navigateur à l'adresse [http://localhost:8080/] pour accéder à l'accueil