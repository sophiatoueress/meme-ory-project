import { parseUrl } from "../../utils/utils";
import { Component } from "../../utils/component";
import "./score.component.scss";
import template from "./score.component.html";

export class ScoreComponent extends Component {
    /* class ScoreComponent constructor */
    constructor() {
        super("score")
        const params = parseUrl();
        this.name = params.name;
        this.size = parseInt(params.size);
        this.time = parseInt(params.time);
    }

    //methods
    getTemplate() {
        return template;
    }

    init() {
        document.getElementById('name').innerText = this.name;
        document.getElementById('size').innerText = this.size;
        document.getElementById('time').innerText = this.time;
    }
}
