export function parseUrl() {
    debugger;
    const url = window.location;
    const query = url.href.split('?')[1] || '';
    const delimiter = '&';
    let result = {}

    let parts = query
        .split(delimiter);
    result = parts.map(kv => kv.split('='))
        .reduce((acc, kv) => {
            const [k, v] = kv;
            result[k] = v;
            return result;
        }, 0);
    return result;
}